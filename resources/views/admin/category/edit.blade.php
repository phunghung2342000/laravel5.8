@extends('admin.layouts.master')

@section('title')
@endsection

@section('content')
    <div class="col-md-6">
        <form action="{{ route('categories.update',['id'=>$category->id]) }}" method="post">
            @csrf
            <div class="form-group">
                <label>Tên danh mục</label>
                <input type="text" class="form-control" placeholder="Nhập tên danh mục" name="name"
                value="{{ $category->name }}">
            </div>
            <div class="form-group">
                <label >Chọn danh mục cha</label>
                <select class="form-control" name="parent_id">
                    <option value="0">chọn</option>
                    {!! $htmlOption !!}
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
