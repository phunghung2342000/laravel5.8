@extends('admin.layouts.master')

@section('title')
@endsection
@section('css')
    <link href="{{ asset('asset/admin/addProduct/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <div class="col-md-6">
        <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Tên sản phẩm</label>
                <input type="text" class="form-control" placeholder="Nhập tên danh mục" name="name">
            </div>

            <div class="form-group">
                <label>Giá sản phẩm</label>
                <input type="text" class="form-control" placeholder="Nhập giá sản phẩm" name="price">
            </div>

            <div class="form-group">
                <label>Ảnh đại diện</label>
                <input type="file" class="form-control-file"  name="feature_image_path">
            </div>


            <div class="form-group">
                <label>Nhập nội dung</label>
                <textarea class="form-control my-editor"  name="contents"></textarea>
            </div>

            <div class="form-group">
                <label >Chọn danh mục</label>
                <select class="form-control chon" name="category_id" multiple="multiple">
                    <option value="0">chọn</option>
                    {!! $htmlOption !!}
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
@section('js')
    <script src="{{ asset('asset/admin/addProduct/select2.min.js') }}"></script>
    <script src="{{ asset("//cdn.tinymce.com/4/tinymce.min.js") }}"></script>

    <script >
        $(function (){
            $(".chon").select2({
                placeholder: "Select a state",
                allowClear: true,
            })
        })


    </script>
    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "textarea.my-editor",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>
@endsection

