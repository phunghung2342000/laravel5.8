@extends('admin.layouts.master')

@section('title')
    Danh mục sản phẩm
@endsection

@section('content')
    <div class = "col-md-12">
        <a href="{{ route('product.create') }}" class="btn btn-success m-2">Add</a>
    </div>

    <div class="col-md-12">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">STT</th>
                <th scope="col">Tên sản phẩm</th>
                <th scope="col">Giá</th>
                <th scope="col">Hình ảnh</th>
                <th scope="col">Danh mục</th>
                <th scope="col">Mô tả sản phẩm</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($product as $key => $value)
                <tr>
                    <th scope="row">{{$key+1}}</th>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->price }}</td>
                    <td><img src="{{ $value->feature_image_path }}"></td>
                    <td>{{ $value->category->name }}</td>
                    <td>{{ $value->content }}</td>
                    <td>
                        <a href="" class="btn btn-dark">Edit</a>
                        <a href="" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class = "col-md-12"></div>
@endsection
