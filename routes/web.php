<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin/pages/index');
});
Route::get('/login','AdminController@login');
Route::post('/login','AdminController@postlogin');
Route::prefix('admin/categories')->group(function () {
    Route::get('/',[
        'as' =>'categories.index',
        'uses' => 'CategoryController@index',
    ]);
    Route::get('/create',[
        'as' =>'categories.create',
        'uses' => 'CategoryController@create',
    ]);
    Route::post('/store',[
        'as' =>'categories.store',
        'uses' => 'CategoryController@store',
    ]);
    Route::get('/edit/{id}',[
        'as' =>'categories.edit',
        'uses' => 'CategoryController@edit',
    ]);
    Route::get('/delete/{id}',[
        'as' =>'categories.delete',
        'uses' => 'CategoryController@delete',
    ]);
    Route::post('/update/{id}',[
        'as' =>'categories.update',
        'uses' => 'CategoryController@update',
    ]);

});
Route::prefix('admin/products')->group(function () {
    Route::get('/',[
        'as' =>'product.index',
        'uses' => 'ProductController@index',
    ]);
    Route::get('/add',[
        'as' =>'product.create',
        'uses' => 'ProductController@create',
    ]);
    Route::post('/store',[
        'as' =>'product.store',
        'uses' => 'ProductController@store',
    ]);

});
