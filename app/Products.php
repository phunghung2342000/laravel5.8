<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = "products";
    protected $guarded =[];
    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }
}
