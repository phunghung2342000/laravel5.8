<?php

namespace App\Http\Controllers;

use App\Category;
use App\Products;
use Illuminate\Http\Request;
use Storage;
class ProductController extends Controller
{
    private $htmtSlelect;
    public function _construct(){
        $this->htmlSlelect = '';
    }
    public function categoryRecuaslve($id,$parentId){
        $data = Category::all();
        foreach ($data as $value){
            if($value['parent_id'] == $id  ){
                if(!empty($parentId) && $parentId == $value['id']){
                    $this->htmtSlelect.= "<option selected value='".$value['id']."'>" .$value['name'].'</option>';
                }else {
                    $this->htmtSlelect .= "<option value='" . $value['id'] . "'>" . $value['name'] . '</option>';
                }

                $this->categoryRecuaslve($value['id'],$parentId);
            }

        }
        return $this->htmtSlelect;
    }
    public function index()
    {
        $product = Products::paginate(5);
        return view('admin.product.index',compact('product'));
    }

    public function create(){
        $htmlOption = $this->categoryRecuaslve(0,'');
        return view('admin.product.add',compact('htmlOption'));
    }

    public function store(Request $request)
    {
        $dataProductCreate = [
            'name' => $request->name,
            'price' => $request->price,
            'content' => $request->contents,
            'user_id' => '1',
            'category_id' => $request->category_id,

        ];
        $file = $request->feature_image_path;
        $fileNameOrigin = $file->getClientOriginalName();
        $fileNameHash = str_random(20) . '.' . $file->getClientOriginalExtension();
        $path = $request->file('feature_image_path')->storeAs('public/product/' . '1', $fileNameHash);
        $data = [
            'file_name' => $fileNameOrigin,
            'file_path' => Storage::url($path)
        ];
        $dataProductCreate['feature_image_path']=$data['file_path'];
        $dataProductCreate['feature_image_name']=$data['file_name'];
        Products::create($dataProductCreate);

    }

}
