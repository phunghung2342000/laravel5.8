<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function login(){

        return view('admin.login');
    }
    public function postlogin(Request $request){
        if(auth()->attempt([
            'email' => $request->email,
            'password' => $request->password
        ])){
            return '1';
        };

    }
}
