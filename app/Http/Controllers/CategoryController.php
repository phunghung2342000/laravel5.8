<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $htmtSlelect;
    public function _construct(){
        $this->htmlSlelect = '';
    }
    public function create(){
        $htmlOption = $this->categoryRecuaslve(0,'');
        return view('admin.category.add',compact('htmlOption'));
    }
    public function categoryRecuaslve($id,$parentId){
        $data = Category::all();
        foreach ($data as $value){
            if($value['parent_id'] == $id  ){
                if(!empty($parentId) && $parentId == $value['id']){
                    $this->htmtSlelect.= "<option selected value='".$value['id']."'>" .$value['name'].'</option>';
                }else {
                    $this->htmtSlelect .= "<option value='" . $value['id'] . "'>" . $value['name'] . '</option>';
                }

                $this->categoryRecuaslve($value['id'],$parentId);
            }

        }
        return $this->htmtSlelect;
    }
    public function index(){
        $category = Category::paginate(5);
        return view('admin.category.index',compact('category'));
    }
    public function store(Request $request){
        Category::create([
            'name' => $request->name,
            'parent_id' => $request->parent_id,
            'slug' => str_slug($request->name)
        ]);
        return redirect(route('categories.index'));
    }
    public function getCategory($parentId){
        $htmlOption = $this->categoryRecuaslve(0,$parentId);
        return $htmlOption;
    }
    public function edit($id){

        $category = Category::find($id);
        $htmlOption = $this->getCategory($category->perent_id);
        return view("admin.category.edit",compact('category','htmlOption'));
    }
    public function update($id,Request $request){
        $category = Category::find($id)->update([
            'name' => $request->name,
            'parent_id' => $request->parent_id,
            'slug' => str_slug($request->name)
        ]);
        return redirect(route('categories.index'));
    }
    public function delete($id){
        Category::find($id)->delete();
        return redirect(route('categories.index'));

    }

}
